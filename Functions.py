class Funciones: #Se declara una clase funciones donde se contienen todas las funciones del programa

    def AñadirMateria(): #Función que añade una nueva materia
        nombreMat = input("Ingresar nombre de la materia: ")

        print("----- Añadir Materia -----") #Este es el menú de la función añadir materia
        print(f"Que desea hacer con {nombreMat}?")
        print("1. Añadir evaluación")
        print("2. Ver evaluaciones ")
        print("3. Eliminar evaluación")
        print("4. Regresar al menu principal")
        opc = int(input("Opción: "))

        if opc == 1:
           Funciones.AñadirEvaluacion(self=())

        return


    def AñadirEvaluacion(self): #Funcion que añade una nueva evaluacion a dentro de alguna materia
        global nombreEval
        nombreEval = input("Ingresar nombre de la evaluación: ")
        global porcentaje
        porcentaje = float(input("Cual es el porcentaje de la evaluación: "))

        print("----- Añadir Evaluación -----") #Menu de la función que añade evaluación
        print(f"Que desea hacer con {nombreEval}?")
        print("1. Añadir nota")
        print("2. Ver y editar notas")
        print("3. Eliminar nota")
        print("4. Regresar a Añadir Materia")
        opc = int(input("Opción: "))

        if opc == 1:
            Funciones.AñadirNota()

        elif opc == 2:
            print(f"El promedio de las notas es: {promedioNotas10}")

        elif opc == 4:
            AñadirMateria()

        return


    def AñadirNota(): #Función que permite añadir nuevas notas a las evaluaciones asignadas
        global promedioNotas10
        x = 0
        retorn = 1
        PN = 0
        while retorn != 2:
            print("----- Añadir Nota -----") #Micro menu de añadir una nota
            evaluacion = float(input(f"{nombreEval} {x+1}: "))
            PN = PN + evaluacion
            x += 1

            print("1. Añadir otra nota?")
            print("2. Ver Promedio")
            print("3. Regresar a Añadir Evaluación")
            retorn = int(input("Opción: "))

            if retorn == 2: #Funcion que saca un pormedio de tus notas en una evaluación
                promedioNotas10 = PN / x
                print(f"El promedio de las notas es: {promedioNotas10}")


            elif retorn == 3:
                Funciones.AñadirMateria()
