import Functions
if __name__ == '__main__': #Clase main

    opc = 0
    while opc != 4:
        print("----- Menu -----") #Menu principal
        print("Que desea hacer?:")
        print("1. Añadir una materia")
        print("2. Ver materias")
        print("3. Eliminar una materia")
        print("4. Salir")
        opc = int(input("Opción: "))

        if opc == 1: #Se ejecuta la funcion "AñadirMAteria"
            Functions.Funciones.AñadirMateria()